const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Membaca input dari terminal
rl.question('Masukkan input dalam format "angka1 operator angka2": ', (input_str) => {
  // Memecah input menjadi tiga bagian: angka1, operator, angka2
  const [angka1, operator, angka2] = input_str.split(' ');

  // Mengubah angka1 dan angka2 menjadi float
  const angka1_float = parseFloat(angka1);
  const angka2_float = parseFloat(angka2);

  let hasil;

  // Melakukan operasi sesuai operator yang diberikan
  switch (operator) {
    case '+':
      hasil = angka1_float + angka2_float;
      break;
    case '-':
      hasil = angka1_float - angka2_float;
      break;
    case '*':
      hasil = angka1_float * angka2_float;
      break;
    case '/':
      hasil = angka1_float / angka2_float;
      break;
    case 'akar':
      hasil = Math.sqrt(angka1_float);
      break;
    case 'luas':
      hasil = angka1_float ** 2;
      break;
    case 'kubus':
      hasil = angka1_float ** 3;
      break;
    case 'tabung':
      hasil = Math.PI * (angka1_float ** 2) * angka2_float;
      break;
    default:
      console.log('Operator yang dimasukkan tidak valid');
      process.exit();
  }

  // Menampilkan hasil kalkulasi
  console.log(hasil);

  rl.close();
});
